import vtk
from math import pi
from sphere_generator import generate_atom_file

class MouseInteractorHighLightActor(vtk.vtkInteractorStyleSwitch):
    def __init__(self, bounds, volume, parent=None):
        if (parent is not None):
            self.parent = parent
        else:
            self.parent = vtk.vtkRenderWindowInteractor()

        # boundaries of region where particle will be placed
        self.bounds = bounds
        # z coordinate to be in mesh
        self.z = self.bounds[4] + 0.5*(self.bounds[5] - self.bounds[4])
        print("z coord: %f" % self.z)

        # total volume of insertion region
        self.volume = volume

        #info related to concentration
        self.rho_p = 2360.0
        # desired concentrations
        self.cv = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        self.cvkg = [self.rho_p * i for i in self.cv]

        self.mass = [self.volume * c for c in self.cvkg]
        print("Total mass of solids in volume: ", self.mass, self.cv)

        pm = pi*0.0003*0.0003*self.z * self.rho_p

        # Desired number of particles for given concentration
        self.target_part_numb = [round(m / pm) for m in self.mass]
        print ("Desired number of particles: ", self.target_part_numb)

        # Observers for custom actions depends on current selected interactor style
        self.SetCurrentStyleToTrackballActor()
        self.style = self.GetCurrentStyle()
        self.style.AddObserver("MiddleButtonReleaseEvent", self.middleButtonPressEvent)
        self.style.AddObserver("LeftButtonPressEvent", self.leftButtonPressEvent)
        self.style.AddObserver("RightButtonPressEvent", self.rightButtonPressEvent)
        # Camera style
        self.SetCurrentStyleToTrackballCamera()
        self.style = self.GetCurrentStyle()

        self.style.AddObserver("RightButtonPressEvent", self.rightButtonPressEvent)
        self.style.AddObserver("KeyPressEvent", self.keyPressEvent)

        self.LastPickedActor = None
        self.LastPickedProperty = vtk.vtkProperty()

    def getSpheresCount(self):
        ac = self.GetDefaultRenderer().GetActors()
        ac.InitTraversal()

        s_count = 0
        not_s_count = 0
        for i in range(ac.GetNumberOfItems()):
            self.NewPickedActor = ac.GetNextActor()
            sphere_ref = self.checkIfSphere()
            if sphere_ref:
                #print "Sphere pos: ", sphere_ref.GetCenter(), sphere_ref.GetRadius()
                s_count += 1
            else:
                not_s_count += 1

        print "Spheres: ", s_count, " others: ", not_s_count
        return s_count

    def getAnySphereRadius(self):
        ac = self.GetDefaultRenderer().GetActors()
        ac.InitTraversal()
        for i in range(ac.GetNumberOfItems()):
            self.NewPickedActor = ac.GetNextActor()
            sphere_ref = self.checkIfSphere()
            if sphere_ref:
                return sphere_ref.GetRadius()

    def getConcentration(self):
        sc = self.getSpheresCount()
        r = self.getAnySphereRadius()
        # total particles volume in system
        svol = pi*r*r*self.z*sc
        print("Current concentration %f, %d " % (svol/self.volume, sc))


    def addSphere(self, pos):
        # new sphere params
        ns = vtk.vtkSphereSource()

        ns.SetRadius(self.getAnySphereRadius())
        ns.SetCenter(pos)
        ns.SetThetaResolution(6)

        ns.Update()

        # new sphere mapper
        m = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            m.SetInput(ns.GetOutput())
        else:
            m.SetInputConnection(ns.GetOutputPort())

        # new actor in the scene
        actor = vtk.vtkActor()
        actor.SetMapper(m)
        actor.GetProperty().SetOpacity(0.5)
        actor.GetProperty().SetColor(0, 0, 1)

        # add actor to renderer
        ren = self.GetDefaultRenderer()
        ren.AddActor(actor)

    def reshapeSpheres(self):
        ac = self.GetDefaultRenderer().GetActors()
        ac.InitTraversal()
        for i in range(ac.GetNumberOfItems()):
            self.NewPickedActor = ac.GetNextActor()
            sphere_ref = self.checkIfSphere()
            if sphere_ref:
                sphere_ref.SetThetaResolution(32)
                sphere_ref.SetPhiResolution(32)

    def generatePositions(self):
        # create list of spheres centres
        spheres = []
        r = self.getAnySphereRadius()

        ac = self.GetDefaultRenderer().GetActors()
        ac.InitTraversal()
        for i in range(ac.GetNumberOfItems()):
            self.NewPickedActor = ac.GetNextActor()
            sphere_ref = self.checkIfSphere()
            if sphere_ref:
                spheres.append(sphere_ref.GetCenter())

        generate_atom_file("Output/atoms.in", spheres, r, self.rho_p, (0, 0, 0))


    def keyPressEvent(self, obj, event):

        key = self.parent.GetKeySym()
        print "Key press event for ", key

        if self.GetCurrentStyle().GetClassName() == 'vtkInteractorStyleTrackballCamera':
            if key == 'i':
                print "Insert new sphere here"

                # convert mouse coordinates to world ('mesh') coordinates
                mousePos = self.parent.GetEventPosition()
                worldPos = vtk.vtkCoordinate()
                worldPos.SetCoordinateSystemToDisplay()
                worldPos.SetValue(mousePos[0], mousePos[1], 0)
                wp = worldPos.GetComputedWorldValue(self.GetDefaultRenderer())

                # new sphere pos will be
                spos = wp[0], wp[1], self.z
                # print mousePos, spos

                self.addSphere(spos)
                self.getConcentration()

            if key == 'o':
                self.reshapeSpheres()

            if key == 'g':
                self.generatePositions()

            if key == 'd':
                self.pick_actor()
                if self.checkIfSphere():
                    print "Remove current sphere actor from this position"
                    self.GetDefaultRenderer().RemoveActor(self.NewPickedActor)
                    self.NewPickedActor = None
                    self.getConcentration()

            if key == 'End':
                print "Get the location of all spheres"

                ac = self.GetDefaultRenderer().GetActors()
                ac.InitTraversal()

                s_cout = 0
                not_s_count = 0
                for i in range(ac.GetNumberOfItems()):
                    self.NewPickedActor = ac.GetNextActor()
                    sphere_ref = self.checkIfSOnMiddleButtonUpphere()
                    if sphere_ref:
                        print "Sphere pos: ", sphere_ref.GetCenter(), sphere_ref.GetRadius()
                        s_cout += 1
                    else:
                        not_s_count += 1

                print "Spheres: ", s_cout
                print "Other objects: ", not_s_count

        self.parent.Render()
        self.OnKeyPress()
        return

    def pick_actor(self):
        clickPos = self.parent.GetEventPosition()

        picker = vtk.vtkPropPicker()
        picker.Pick(clickPos[0], clickPos[1], 0, self.GetDefaultRenderer())

        # get the new
        self.NewPickedActor = picker.GetActor()

        # If something was selected
        if self.NewPickedActor:
            # If we picked something before, reset its property
            if self.LastPickedActor:
                self.LastPickedActor.GetProperty().DeepCopy(self.LastPickedProperty)

            sphereRef = self.checkIfSphere()
            if sphereRef:
                # Save the property of the picked actor so that we can
                # restore it next time
                self.LastPickedProperty.DeepCopy(self.NewPickedActor.GetProperty())
                # Highlight the picked actor by changing its properties
                self.NewPickedActor.GetProperty().SetColor(1.0, 0.0, 0.0)
                self.NewPickedActor.GetProperty().SetDiffuse(1.0)
                self.NewPickedActor.GetProperty().SetSpecular(0.0)
                self.GetDefaultRenderer().Render()
                # save the last picked actor
                self.LastPickedActor = self.NewPickedActor
        self.parent.Render()



    def middleButtonPressEvent(self, obj, event):
        if self.GetCurrentStyle().GetClassName() == 'vtkInteractorStyleTrackballActor':
            self.pick_actor()
            # self.GetCurrentStyle().SetPickColor(1,0,0)
            self.GetCurrentStyle().OnMiddleButtonDown()
            self.GetCurrentStyle().OnMiddleButtonUp()
            self.setPosition()

        return

    def rightButtonPressEvent(self, obj, event):

        if self.GetCurrentStyle().GetClassName() == 'vtkInteractorStyleTrackballCamera':
            print "RB press event Camera"

        if self.GetCurrentStyle().GetClassName() == 'vtkInteractorStyleTrackballActor':
            self.pick_actor()

        self.parent.Render()
        #self.OnRightButtonDown()
        return

    def setPosition(self):
        sphere_ref = self.checkIfSphere()
        if sphere_ref:
            current_position = sphere_ref.GetCenter()
            ap = self.NewPickedActor.GetPosition()
            t = vtk.vtkTransform()
            t.Translate(ap[0], ap[1], 0)

            transformFilter = vtk.vtkTransformPolyDataFilter()
            transformFilter.SetInputConnection(sphere_ref.GetOutputPort())
            transformFilter.SetTransform(t)
            transformFilter.Update()

            new_pos = transformFilter.GetOutput().GetCenter()
            sphere_ref.SetCenter(new_pos[0], new_pos[1], self.z)
            delta = [0] * 3
            for i in range(3):
                delta[i] = current_position[i] - new_pos[i]

            sphere_ref.Modified()
            sphere_ref.Update()
            #print current_position, new_pos, sphere_ref.GetCenter(), delta
            self.NewPickedActor.GetMapper().Modified()
            self.NewPickedActor.GetMapper().Update()
        self.parent.Render()


    def leftButtonPressEvent(self, obj, event):
        self.pick_actor()
        self.parent.Render()


    def checkIfSphere(self):
        algorithm = self.NewPickedActor.GetMapper().GetInputConnection(0, 0).GetProducer()
        return vtk.vtkSphereSource.SafeDownCast(algorithm)



