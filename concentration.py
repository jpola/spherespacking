'''
Obliczamy ilosc czastek ktore musimy uzyc w symulacji aby otrzymac dana koncentracje
'''

# gestosc czastek
rho_p = 2360
# promien czastek
r = 0.0003
#grubosc siatki
dz = 0.000199 - 1e-6
#masa czastki 2.5D
mp = rho_p * dz * r*r * 3.1415
print ("Mass of the single particle ", mp)

# koncentracja
cv = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
cvkg = [ rho_p * i for i in cv]
print ("Concentrations [kg/m^3]: ", cvkg)

# objetosc siatki [2mm, 4mm, 8mm], z danych checkMesh
#vm = [3.916994e-08, 7.92491e-08, 1.77902e-07]
vm = [6.8101602173073994e-09, 0, 0]

#masa substancji w danej objetosci
mass_vm = []
for v in vm:
    mass = [v*c for c in cvkg ]
    print("Total mass of solids for volume %e " % v, mass)
    mass_vm.append(mass)

Np = []
for m in mass_vm:
    n = [mi/mp for mi in m]
    Np.append(n)

print ("Concentrations: ", cv)
print ("corresponding number of particles in given geom:")
print ("\t2mm channel:")
x = [int(round(n)) for n in Np[0]]
# print ("\t\t", Np[0])
print ("\t\t", x)
print ("\t4mm channel:")
x = [int(round(n)) for n in Np[1]]
print ("\t\t", x)
print ("\t8mm channel:")
x = [int(round(n)) for n in Np[2]]
# print ("\t\t", Np[2])
print ("\t\t", x)




