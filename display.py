from stl import mesh
import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot
from scipy.spatial import kdtree
from operator import itemgetter
import random

import vtk


def loadSTL(stl_file):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(stl_file)

    reader.Update()

    polydata = reader.GetOutput()

    if polydata.GetNumberOfPoints() == 0:
        raise ValueError\
                ("No point data could be loaded from " + stl_file)
        return None
    return polydata


def create_mesh(mesh1, mesh2):
    input1 = vtk.vtkPolyData()
    input2 = vtk.vtkPolyData()

    input1.ShallowCopy(mesh1)
    input2.ShallowCopy(mesh2)

    appendFilter = vtk.vtkAppendPolyData()
    if vtk.VTK_MAJOR_VERSION <= 5:
        appendFilter.AddInputConnection(input1.GetProducerPort())
        appendFilter.AddInputConnection(input2.GetProducerPort())
    else:
        appendFilter.AddInputData(input1)
        appendFilter.AddInputData(input2)

    appendFilter.Update()

    #  Remove any duplicate points.
    cleanFilter = vtk.vtkCleanPolyData()
    cleanFilter.SetInputConnection(appendFilter.GetOutputPort())
    cleanFilter.Update()

    return cleanFilter.GetOutput()

def create_mesh_lr(stl_right, stl_left):
    left = mesh.Mesh.from_file(stl_right)
    right = mesh.Mesh.from_file(stl_left)
    data = np.concatenate((left.data, right.data))
    return mesh.Mesh(data)

def plot3D_pts(points, min, max, every=50):
    figure = pyplot.figure()
    axes = mplot3d.Axes3D(figure)
    axes.auto_scale_xyz([min[0], max[0]], [min[1], max[1]],
                        [min[2], max[2]])
    for p in pts[::every]:
        axes.scatter(p[0], p[1], p[2], color='black')
    return axes


def plot3D_spheres(points, min, max, axes=None):
    if axes:
        for p in points:
            axes.scatter(p[0], p[1], p[2], color='red')
    else:
        plot3D_pts(points, min, max, 1)


def plot2D_pts(points, marker_size = 1):
    xy = points[:, :2]
    a, b = list(map(itemgetter(0), xy)), list(map(itemgetter(1), xy))

    figure, ax = pyplot.subplots()
    ax.scatter(a,b, color='black', s = marker_size)


def plot2D_bounds_and_spheres(bounds_pts, spheres_pts, marker_size = 1):
    xy = bounds_pts[:, :2]
    a, b = list(map(itemgetter(0), xy)), list(map(itemgetter(1), xy))

    figure, ax = pyplot.subplots()
    ax.scatter(a,b, color='black', s = marker_size)

    sx, sy = list(map(itemgetter(0), spheres_pts)), \
             list(map(itemgetter(1), spheres_pts))
    ax.scatter(sx,sy, color='red', s = 3)


def randomize_position(bounds):
    x = random.uniform(bounds[0], bounds[1])
    y = random.uniform(bounds[2], bounds[3])
    z = random.uniform(bounds[4], bounds[5])
    return x, y, z

def randomize_position_2D(bounds):
    x = random.uniform(bounds[0], bounds[1])
    y = random.uniform(0.05, bounds[3])
    z = (bounds[4] + bounds[5])*0.5
    # print ("Warning bounds overriden")
    return x, y, z

def get_bounds(stl, buffer=(0, 0, 0, 0, 0, 0)):
    return stl.min_[0]+buffer[0], stl.max_[0]-buffer[1], \
           stl.min_[1]+buffer[2], stl.max_[1]-buffer[3], \
           stl.min_[2]+buffer[4], stl.max_[2]-buffer[5]

def generate_spheres(mesh_file1, mesh_file2, radius=3e-4, buffer=0.25e-4, N=750, retries=10):

    # sphere_true_radius = 3e-4
    # sphere_buffer = 0.25e-4
    sphere_radius = radius + buffer

    stl_mesh = create_mesh_lr(mesh_file1, mesh_file2)
    bounds = get_bounds(stl_mesh, (0, 0, 0, 2*sphere_radius, 0, 0))

    print ("bounds : ", bounds)

    pts = np.zeros(shape=(stl_mesh.vectors.shape[1], 3))
    pts = np.asarray([np.mean(v, axis=0) for v in stl_mesh.vectors])
    pts = np.asarray( [[v[0], v[1], (bounds[4] + bounds[5])*0.5] for v in pts])

    pts_tree = kdtree.KDTree(pts)

    spheres = []

    for n in range(N):
        for i in range(retries):
            sphere_pos = randomize_position_2D(bounds)
            with_bounds_intersection = pts_tree.query_ball_point(sphere_pos, r=sphere_radius)
            if spheres:
                with_others_intersection = spheres_tree.query_ball_point(sphere_pos, r=2.3*sphere_radius)
                if not with_others_intersection and not with_bounds_intersection:
                    spheres.append(sphere_pos)
                    spheres_tree = kdtree.KDTree(spheres)
                    break
                else:
                    continue
            else:
                if not with_bounds_intersection:
                    spheres.append(sphere_pos)
                    spheres_tree = kdtree.KDTree(spheres)
                    break
    return spheres

def addPoint(renderer, p, c=(0, 255, 0)):
    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()
    id = points.InsertNextPoint(p)
    vertices.InsertNextCell(1)
    vertices.InsertCellPoint(id)

    # Setup the colors array
    colors = vtk.vtkUnsignedCharArray()
    colors.SetNumberOfComponents(3)
    colors.SetName("Colors")
    # Add the colors we created to the colors array
    colors.InsertNextTuple3(c[0], c[1], c[2])

    point = vtk.vtkPolyData()
    point.SetPoints(points)
    point.SetVerts(vertices)

    point.GetCellData().SetScalars(colors)

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(point)
    else:
        mapper.SetInputData(point)



    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(20)

    renderer.AddActor(actor)


def addLine(renderer, p1, p2, c=(255, 0, 0)):
    linepts = vtk.vtkPoints()
    linepts.InsertNextPoint(p1)
    linepts.InsertNextPoint(p2)

    # Setup the colors array
    colors = vtk.vtkUnsignedCharArray()
    colors.SetNumberOfComponents(3)
    colors.SetName("Colors")
    # Add the colors we created to the colors array
    colors.InsertNextTuple3(c[0], c[1], c[2])

    _line = vtk.vtkLine()
    _line.GetPointIds().SetId(0, 0)
    _line.GetPointIds().SetId(1, 1)

    lines = vtk.vtkCellArray()
    lines.InsertNextCell(_line)

    linesPolyData = vtk.vtkPolyData()
    linesPolyData.SetPoints(linepts)
    linesPolyData.SetLines(lines)

    linesPolyData.GetCellData().SetScalars(colors)

    linemapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        linemapper.SetInput(linesPolyData)
    else:
        linemapper.SetInputData(linesPolyData)

    lactor = vtk.vtkActor()
    lactor.SetMapper(linemapper)

    renderer.AddActor(lactor)


def sweep_outer_points(spheres, obbTree, direction=1.):

    good_spheres=[]

    for s in spheres:
        poi = vtk.vtkPoints()
        source = s

        destination = (s[0] + direction*2*0.015, s[1], s[2])

        result = obbTree.IntersectWithLine(source, destination, poi, None)

        if result == -1:
        # if poi.GetNumberOfPoints() == 1:
            pointsVTKIntersectionData = poi.GetData()
            noPointsVTKIntersection = pointsVTKIntersectionData.GetNumberOfTuples()
            pointsIntersection = []
            for idx in range(noPointsVTKIntersection):
                _tup = pointsVTKIntersectionData.GetTuple3(idx)
                pointsIntersection.append(_tup)

            distance = [ np.fabs(_tup[i] - s[i])for i in range(len(_tup))]

            print(distance)

            # if distance[0] < 1e-5:
            #     addLine(renderer, source, destination, (0, 0, 255))
            #     addPoint(renderer, pointsIntersection[0], (0, 0, 255))

            addLine(renderer, source, destination)
            addPoint(renderer, pointsIntersection[0])
            good_spheres.append(s)
    return good_spheres

def generate_atom_file(fname, pos, radius, density = 2360, velocity = (0, 0, 0), omega = (0, 0, 0)):
    fout = open(fname, "w")

    index = 1
    for p in pos:
        create_string = "create_atoms 1 single %lg %lg %lg units box\n" % (p[0], p[1], p[2])
        index += 1
        fout.write(create_string)
    fout.write("\n")

    #density = 2360 #sand
    vx, vy, vz = velocity
    wx, wy, wz = omega

    for i in range(index):
        set_atom_string = "set atom %d diameter %lg density %lg vx %lg vy %lg vz %lg omegax %lg omegay %lg omegaz %lg\n" % \
                          (i+1,2.*radius, density, vx, vy, vz, wx, wy, wz)
        fout.write(set_atom_string)

    fout.close()


sphere_true_radius = 3e-4
sphere_buffer = 0.1e-4
sphere_radius = sphere_true_radius + sphere_buffer

spheres = generate_spheres('left.stl', 'right.stl',
                           sphere_true_radius, sphere_buffer, 2000, 50)


right = loadSTL('right.stl')
left = loadSTL('left.stl')

vtk_mesh = create_mesh(right, left)

obbTree = vtk.vtkOBBTree()
obbTree.SetDataSet(vtk_mesh)
obbTree.BuildLocator()

renderer = vtk.vtkRenderer()

spheres = sweep_outer_points(spheres, obbTree, 1.)
spheres = sweep_outer_points(spheres, obbTree, -1.)


print ("Generated %d spheres"%len(spheres))

generate_atom_file("atoms.in", spheres, sphere_true_radius, density=2360, velocity=(0, -0.0025, 0))

sphere_point_loc = vtk.vtkPoints()
sphere_vertices = vtk.vtkCellArray()
sphere_locations = vtk.vtkPolyData()
sphere_locations.SetPoints(sphere_point_loc)
sphere_locations.SetVerts(sphere_vertices)

vtk_spheres = []
for s in spheres:
    id = sphere_point_loc.InsertNextPoint(s)
    sphere_vertices.InsertNextCell(1)
    sphere_vertices.InsertCellPoint(id)

    _sphere = vtk.vtkSphereSource()
    _sphere.SetCenter(s)
    _sphere.SetRadius(sphere_true_radius)
    _sphere.SetThetaResolution(32)
    _sphere.Update()
    vtk_spheres.append(_sphere)


mesh_mapper = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
    mesh_mapper.SetInput(vtk_mesh)
else:
    mesh_mapper.SetInputData(vtk_mesh)
mesh_actor = vtk.vtkActor()
mesh_actor.SetMapper(mesh_mapper)


s_mappers = []
s_actors = []
for s in vtk_spheres:
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(s.GetOutput())
    else:
        mapper.SetInputData(s.GetOutput())

    s_mappers.append(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetOpacity(0.5)
    s_actors.append(actor)



renderer.AddActor(mesh_actor)
for sa in s_actors:
    renderer.AddActor(sa)




renderer.SetBackground(1.0, 1.0, 1.0)
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renderer)
# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)


# enable user interface interactor
iren.Initialize()
renWin.Render()
iren.Start()

