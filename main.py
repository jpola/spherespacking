import vtk
import meshio
import meshmanip
import sphere_generator
import MouseInteractorHighLightActor as miha
import atexit

sphere_true_radius = 3e-4
sphere_buffer = 1e-5
sphere_radius = sphere_true_radius + sphere_buffer

data_dir = 'Data/2mm'


np_mesh1 = meshio.load_stl(data_dir + '/right.stl', 'numpy')
np_mesh2 = meshio.load_stl(data_dir + '/left.stl', 'numpy')
stl_mesh = meshmanip.merge_meshes((np_mesh1, np_mesh2))
bounds = meshmanip.get_bounds(stl_mesh, (0, 0, 0.075, 2*sphere_radius, 0, 0))

print ("bounds : ", bounds)

spheres = sphere_generator.generate_spheres(stl_mesh, bounds,
                                            sphere_true_radius, sphere_buffer,
                                            2000, 100)


right = meshio.load_stl(data_dir + '/right.stl')
left = meshio.load_stl(data_dir + '/left.stl')

vtk_mesh = meshmanip.merge_meshes((right, left))

total_volume = meshmanip.calculate_volume(vtk_mesh)

renderer = vtk.vtkRenderer()
#renderer.GetActiveCamera().ParallelProjectionOn()

spheres, vol = sphere_generator.sweep_outer_spheres_by_segment(vtk_mesh, spheres, bounds, renderer)

print ("Total volume of mesh: %f" % total_volume)
print ("Volume of region in which spheres are gen %f, fraction of total vol %f" % (vol, vol/total_volume))

print ("Generated %d spheres"%len(spheres))

sphere_generator.generate_atom_file("Output/atoms.in", spheres, sphere_true_radius, density=2360, velocity=(0, -0.0025, 0))

sphere_point_loc = vtk.vtkPoints()
sphere_vertices = vtk.vtkCellArray()
sphere_locations = vtk.vtkPolyData()
sphere_locations.SetPoints(sphere_point_loc)
sphere_locations.SetVerts(sphere_vertices)

vtk_spheres = []
for s in spheres:
    id = sphere_point_loc.InsertNextPoint(s)
    sphere_vertices.InsertNextCell(1)
    sphere_vertices.InsertCellPoint(id)

    _sphere = vtk.vtkSphereSource()
    _sphere.SetCenter(s)
    _sphere.SetRadius(sphere_true_radius)
    _sphere.SetThetaResolution(32)
    _sphere.Update()
    vtk_spheres.append(_sphere)


mesh_mapper = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
    mesh_mapper.SetInput(vtk_mesh)
else:
    mesh_mapper.SetInputData(vtk_mesh)
mesh_actor = vtk.vtkActor()
mesh_actor.SetMapper(mesh_mapper)


s_mappers = []
s_actors = []
for s in vtk_spheres:
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(s.GetOutput())
    else:
        mapper.SetInputConnection(s.GetOutputPort())

    s_mappers.append(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetOpacity(0.5)
    s_actors.append(actor)



renderer.AddActor(mesh_actor)
for sa in s_actors:
    renderer.AddActor(sa)

def save_spheres_config():
    print "Exit function called"

atexit.register(save_spheres_config)

renderer.SetBackground(1.0, 1.0, 1.0)
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renderer)
# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# add the custom style
style = miha.MouseInteractorHighLightActor(bounds, vol, parent=iren)
style.SetDefaultRenderer(renderer)
iren.SetInteractorStyle(style)

# enable user interface interactor
iren.Initialize()
renWin.Render()
iren.Start()
