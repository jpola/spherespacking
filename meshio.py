import vtk
from stl import mesh

def load_stl(stl_file, format='vtk'):
    if format == 'vtk':
        return load_to_vtk(stl_file)
    if format == 'numpy':
        return mesh.Mesh.from_file(stl_file)



def load_to_vtk(stl_file):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(stl_file)

    reader.Update()

    polydata = reader.GetOutput()

    if polydata.GetNumberOfPoints() == 0:
        raise ValueError \
            ("No point data could be loaded from " + stl_file)
        return None
    return polydata
