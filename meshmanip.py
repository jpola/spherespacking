import vtk
from stl import mesh
import numpy as np


def merge_meshes(meshes):

    # Check tuple consistency
    mtype = type(meshes[0])

    for m in meshes:
        if isinstance(m, mtype):
            continue
        else:
            raise TypeError("Inconsistent objects in tuple")

    if mtype is mesh.Mesh:
        data = np.concatenate([m.data for m in meshes])
        return mesh.Mesh(data)
    # vtk 6 haven't got separate classess for objects
    elif (mtype is vtk.vtkPolyData) or meshes[0].GetClassName() == 'vtkPolyData':
        return merge_vtk_meshes(meshes)
    else:
        cname = meshes[0].GetClassName()
        print cname
        raise TypeError("Unhandled mesh type")


def merge_vtk_meshes(meshes):

    # inputs = [vtk.vtkPolyData() for i in range(len(meshes))]
    inputs = [None] * len(meshes)

    for i in range(len(inputs)):
        inputs[i] = vtk.vtkPolyData()
        inputs[i].ShallowCopy(meshes[i])

    append_filter = vtk.vtkAppendPolyData()
    if vtk.VTK_MAJOR_VERSION <= 5:
        for i in range(len(inputs)):
            append_filter.AddInputConnection(inputs[i].GetProducerPort())
    else:
        for i in range(len(inputs)):
            append_filter.AddInputData(inputs[i])

    append_filter.Update()

    #  Remove any duplicate points.
    clean_filter = vtk.vtkCleanPolyData()
    clean_filter.SetInputConnection(append_filter.GetOutputPort())
    clean_filter.Update()

    return clean_filter.GetOutput()


def get_bounds(_mesh, buffer = (0, 0, 0, 0, 0, 0)):
    # if isinstance(_mesh, mesh.Mesh):
    if type(_mesh) is mesh.Mesh:
        return _mesh.min_[0]+buffer[0], _mesh.max_[0]-buffer[1], \
               _mesh.min_[1]+buffer[2], _mesh.max_[1]-buffer[3], \
               _mesh.min_[2]+buffer[4], _mesh.max_[2]-buffer[5]
    elif isinstance(_mesh, vtk.vtkPolyData) or _mesh.GetClassName() == 'vtkPolyData':
        bounds = _mesh.GetBounds()
        return bounds[0] + buffer[0], bounds[1] - buffer[1], \
               bounds[2] + buffer[2], bounds[3] - buffer[3], \
               bounds[4] + buffer[4], bounds[5] - buffer[5]


def calculate_volume(mesh):
    mp = vtk.vtkMassProperties()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mp.SetInput(mesh)
    else:
        mp.SetInputData(mesh)
    mp.Update()
    return mp.GetVolume()