import random


def randomize_position(bounds, dim=2):
    if dim == 2:
        return randomize_position_2d(bounds)
    elif dim == 3:
        return randomize_position_3d(bounds)
    else:
        raise ValueError("Wrong dimension given dim=%d" % dim)


def randomize_position_2d(bounds):
    x = random.uniform(bounds[0], bounds[1])
    y = random.uniform(bounds[2], bounds[3])
    z = (bounds[4] + bounds[5]) * 0.5
    return x, y, z


def randomize_position_3d(bounds):
    x = random.uniform(bounds[0], bounds[1])
    y = random.uniform(bounds[2], bounds[3])
    z = random.uniform(bounds[4], bounds[5])
    return x, y, z
