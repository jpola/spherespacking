import meshio
import meshmanip
import randomize

import numpy as np
from scipy.spatial import kdtree
import vtk

def generate_spheres(mesh, bounds = (0, 0, 0, 0, 0, 0), radius=3e-4, buffer=0.25e-4, n=750, retries=10):

    # sphere_true_radius = 3e-4
    # sphere_buffer = 0.25e-4
    sphere_radius = radius + buffer

    pts = np.zeros(shape=(mesh.vectors.shape[1], 3))
    pts = np.asarray([np.mean(v, axis=0) for v in mesh.vectors])
    pts = np.asarray( [[v[0], v[1], (bounds[4] + bounds[5])*0.5] for v in pts])

    pts_tree = kdtree.KDTree(pts)

    spheres = []

    for n in range(n):
        for i in range(retries):
            sphere_pos = randomize.randomize_position_2d(bounds)
            with_bounds_intersection = pts_tree.query_ball_point(sphere_pos, r=sphere_radius)
            if spheres:
                with_others_intersection = spheres_tree.query_ball_point(sphere_pos, r=2.0*sphere_radius)
                if not with_others_intersection and not with_bounds_intersection:
                    spheres.append(sphere_pos)
                    spheres_tree = kdtree.KDTree(spheres)
                    break
                else:
                    continue
            else:
                if not with_bounds_intersection:
                    spheres.append(sphere_pos)
                    spheres_tree = kdtree.KDTree(spheres)
                    break
    return spheres



def sweep_outer_spheres(vtk_mesh, spheres, ren=None):

    obbTree = vtk.vtkOBBTree()
    obbTree.SetDataSet(vtk_mesh.GetOutput())
    obbTree.BuildLocator()

    if ren is not None:
        pd = vtk.vtkPolyData()
        obbTree.GenerateRepresentation(0, pd)

        tm = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            tm.SetInputConnection(pd.GetProducerPort())
        else:
            tm.SetInputData(pd)

        ta = vtk.vtkActor()
        ta.SetMapper(tm)
        ta.GetProperty().SetInterpolationToFlat()
        ta.GetProperty().SetRepresentationToWireframe();

        ren.AddActor(ta)

    spheres = sweep_side(spheres, obbTree, 1.)

    return spheres


def sweep_out_of_box(bbox, pts, ren):
    interior_pts = []
    for p in pts:
        if bbox.ContainsPoint(pts):
            interior_pts.append(p)
    return interior_pts


def check_bound(pos, min, max):
    return min < pos < max

def sweep_outer_spheres_with_box(box, spheres, ren=None):

    bds = [0 for i in range(6)]
    box.GetOutput().GetBounds(bds)

    if ren is not None:
        _b = vtk.vtkCubeSource()
        _b.SetBounds(bds)
        tm = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            tm.SetInput(_b.GetOutput())
        else:
            tm.SetInputConnection(_b.GetOutputPort())

        ta = vtk.vtkActor()
        ta.SetMapper(tm)
        ta.GetProperty().SetInterpolationToFlat()
        ta.GetProperty().SetRepresentationToWireframe();
        ta.GetProperty().SetColor(1, 0, 0)

        ren.AddActor(ta)

    interior_pts = []
    for s in spheres:
        if check_bound(s[0], bds[0], bds[1]) and  \
            check_bound(s[1], bds[2], bds[3]) and \
                check_bound(s[2], bds[4], bds[5]):
            interior_pts.append(s)

    return interior_pts


def sweep_outer_spheres_by_segment(vtk_mesh, spheres, bounds = (0, 0, 0, 0, 0, 0), ren=None):

    _bounds = list(meshmanip.get_bounds(vtk_mesh))
    #Override y bounds!
    _bounds[2] = bounds[2]
    _bounds[3] = bounds[3]

    n_segments = 10

    y = np.linspace(_bounds[2], _bounds[3], n_segments)

    _out_spheres = []

    mesh_interior_volume = 0.0

    for i in range(y.size-1):
    # for i in range(len(y)-2, len(y)-1):
        box = vtk.vtkBox()
        box.SetBounds(_bounds[0], _bounds[1], y[i], y[i+1], _bounds[4], _bounds[5])

        clipper = vtk.vtkClipPolyData()
        clipper.InsideOutOn()
        if vtk.VTK_MAJOR_VERSION <= 5:
            clipper.SetInput(vtk_mesh)
        else:
            clipper.SetInputData(vtk_mesh)
        clipper.SetClipFunction(box)
        clipper.Update()

        #Calc volume of segment
        mp = vtk.vtkMassProperties()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mp.SetInput(clipper.GetOutput())
        else:
            mp.SetInputData(clipper.GetOutput())

        mp.Update()
        mesh_interior_volume += mp.GetVolume()

        _in_spheres = spheres[:]
        spheres_in_section = sweep_outer_spheres_with_box(clipper, _in_spheres, ren)

        #spheres_in_section = sweep_outer_spheres(clipper, _in_spheres, ren)
        _out_spheres.append(spheres_in_section)


    spheres_set = []
    for _s_list in _out_spheres:
        for _s in _s_list:
            spheres_set.append(_s)
    spheres = list(set(spheres_set))
    return spheres, mesh_interior_volume



def sweep_side(spheres, otree, dir, l=0.03):

    good_spheres = []

    for s in spheres:
        poi = vtk.vtkPoints()
        source = s

        destination = (s[0] + dir * l, s[1], s[2])

        #result = otree.IntersectWithLine(source, destination, poi, None)
        result = otree.InsideOrOutside(s)

        if result == -1:
            print result
            # if poi.GetNumberOfPoints() == 1:
            # pointsVTKIntersectionData = poi.GetData()
            # noPointsVTKIntersection = pointsVTKIntersectionData.GetNumberOfTuples()
            # pointsIntersection = []
            # for idx in range(noPointsVTKIntersection):
            #     _tup = pointsVTKIntersectionData.GetTuple3(idx)
            #     pointsIntersection.append(_tup)
            #
            # distance = [np.fabs(_tup[i] - s[i]) for i in range(len(_tup))]
            #
            # print(distance)
            #
            # # if distance[0] < 1e-5:
            # #     addLine(renderer, source, destination, (0, 0, 255))
            # #     addPoint(renderer, pointsIntersection[0], (0, 0, 255))
            #
            # addLine(renderer, source, destination)
            # addPoint(renderer, pointsIntersection[0])
            good_spheres.append(s)
    return good_spheres

def generate_atom_file(fname, pos, radius, density = 2360, velocity = (0, 0, 0), omega = (0, 0, 0)):
    fout = open(fname, "w")

    index = 1
    for p in pos:
        create_string = "create_atoms 1 single %lg %lg %lg units box\n" % (p[0], p[1], p[2])
        index += 1
        fout.write(create_string)
    fout.write("\n")

    #density = 2360 #sand
    vx, vy, vz = velocity
    wx, wy, wz = omega

    for i in range(index):
        set_atom_string = "set atom %d diameter %lg density %lg vx %lg vy %lg vz %lg omegax %lg omegay %lg omegaz %lg\n" % \
                          (i+1, 2.*radius, density, vx, vy, vz, wx, wy, wz)
        fout.write(set_atom_string)

    fout.close()

def addPoint(renderer, p, c=(0, 255, 0)):
    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()
    id = points.InsertNextPoint(p)
    vertices.InsertNextCell(1)
    vertices.InsertCellPoint(id)

    # Setup the colors array
    colors = vtk.vtkUnsignedCharArray()
    colors.SetNumberOfComponents(3)
    colors.SetName("Colors")
    # Add the colors we created to the colors array
    colors.InsertNextTuple3(c[0], c[1], c[2])

    point = vtk.vtkPolyData()
    point.SetPoints(points)
    point.SetVerts(vertices)

    point.GetCellData().SetScalars(colors)

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(point)
    else:
        mapper.SetInputData(point)



    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(20)

    renderer.AddActor(actor)


def addLine(renderer, p1, p2, c=(255, 0, 0)):
    linepts = vtk.vtkPoints()
    linepts.InsertNextPoint(p1)
    linepts.InsertNextPoint(p2)

    # Setup the colors array
    colors = vtk.vtkUnsignedCharArray()
    colors.SetNumberOfComponents(3)
    colors.SetName("Colors")
    # Add the colors we created to the colors array
    colors.InsertNextTuple3(c[0], c[1], c[2])

    _line = vtk.vtkLine()
    _line.GetPointIds().SetId(0, 0)
    _line.GetPointIds().SetId(1, 1)

    lines = vtk.vtkCellArray()
    lines.InsertNextCell(_line)

    linesPolyData = vtk.vtkPolyData()
    linesPolyData.SetPoints(linepts)
    linesPolyData.SetLines(lines)

    linesPolyData.GetCellData().SetScalars(colors)

    linemapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        linemapper.SetInput(linesPolyData)
    else:
        linemapper.SetInputData(linesPolyData)

    lactor = vtk.vtkActor()
    lactor.SetMapper(linemapper)

    renderer.AddActor(lactor)